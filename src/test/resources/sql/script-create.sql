CREATE TABLE BABY_NAMES
(
  YEAR VARCHAR(2000),
  FNAME VARCHAR(2000),
  PERCENT VARCHAR(2000),
  SEX VARCHAR(2000)
);

CREATE TABLE INT_MESSAGE
(
  MESSAGE_ID CHAR(36) NOT NULL,
  REGION VARCHAR(100) NOT NULL,
  CREATED_DATE TIMESTAMP NOT NULL,
  MESSAGE_BYTES VARBINARY(10000),
  CONSTRAINT MESSAGE_PK PRIMARY KEY (MESSAGE_ID, REGION)
);
CREATE INDEX INT_MESSAGE_IX1 ON INT_MESSAGE (CREATED_DATE);

CREATE TABLE INT_MESSAGE_GROUP
(
  GROUP_KEY CHAR(36) NOT NULL,
  REGION VARCHAR(100) NOT NULL,
  MARKED BIGINT,
  COMPLETE BIGINT,
  LAST_RELEASED_SEQUENCE BIGINT,
  CREATED_DATE TIMESTAMP NOT NULL,
  UPDATED_DATE TIMESTAMP DEFAULT NULL,
  CONSTRAINT MESSAGE_GROUP_PK PRIMARY KEY (GROUP_KEY, REGION)
);


CREATE TABLE INT_GROUP_TO_MESSAGE
(
  GROUP_KEY CHAR(36) NOT NULL,
  MESSAGE_ID CHAR(36) NOT NULL,
  REGION VARCHAR(100) NOT NULL,
  CONSTRAINT GROUP_TO_MESSAGE_PK PRIMARY KEY (GROUP_KEY, MESSAGE_ID, REGION)
);
