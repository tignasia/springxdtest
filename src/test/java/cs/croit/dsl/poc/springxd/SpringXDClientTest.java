package cs.croit.dsl.poc.springxd;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.dbunit.DatabaseTestCase;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.csv.CsvDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.hateoas.PagedResources;
import org.springframework.xd.rest.client.impl.SpringXDTemplate;
import org.springframework.xd.rest.domain.DetailedContainerResource;
import org.springframework.xd.rest.domain.StreamDefinitionResource;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

//import org.springframework.xd.dirt.plugins.AbstractPlugin;

public class SpringXDClientTest extends DatabaseTestCase  /*extends TestCase*/ {

    public static final String BABY_NAMES_1 = "BabyNames1";
    public static final String BABY_NAMES_2 = "BabyNames2";
    public static final String BABY_NAMES_3 = "BabyNames3";
    public static final String BABY_NAMES_4 = "BabyNames4";
    public static final String BABY_NAMES_5 = "BabyNames5";

  //  private static final String SPRING_XD_URL = "http://localhost:9393";


    // public static final String SPRING_XD_HSQLDB_URL = "jdbc:hsqldb:hsql://localhost:9101/xdjob;sql.enforce_strict_size=true;hsqldb.tx=mvccs";
    private String SPRING_XD_DB_URL = "jdbc:mysql://localhost:3306/SpringBatch";
    //   public static final String SPRING_XD_JDBC_DRIVER ="org.hsqldb.jdbcDriver";
    public static final String SPRING_XD_JDBC_DRIVER = "com.mysql.jdbc.Driver";

    //public static final String HSQL_USER_NAME ="sa";
    public static final String SPRING_XD_DB_USER_NAME = "batch";
    public static final String SPRING_XD_DB_PASSWORD = "batch";
    public static final String BABY_NAMES1_CSV = "baby-names1.csv";
    public static final String BABY_NAMES2_CSV = "baby-names2.csv";
    public static final String BABY_NAMES3_CSV = "baby-names3.csv";
    public static final String BABY_NAMES4_CSV = "baby-names4.csv";
    public static final String BABY_NAMES5_CSV = "baby-names5.csv";

     private String localResourcesPath;
    //private String scanDirPath;
   // private String resourcesURL;
    private String targetResourcesPath = "C:/Users/tomasz/VirtualBoxShare/";
    private String scanDirPath =  targetResourcesPath + "scanDir/";   /*resourcesPath + "scanDir/"*/;

    private String targetResource = "/media/sf_VirtualBoxShare/";
    private String targetResourcesURL = "file://" + targetResource;
    private String targetScanPath = targetResource + "scanDir/";

   // "/media/sf_VirtualBoxShare/

    //   private  JdbcDatabaseTester databaseTester;
    private IDatabaseConnection databaseConnection;
    private Connection jdbcConnection;
    private SpringXDTemplate xdTemplate;
    private String httpURL;
    private int httpPort;

    //    <profile>
//    <id>singleNode</id>
//    <activation>
//    <activeByDefault>true</activeByDefault>
//    </activation>
//    <properties>
//    <springxd-lib-dir>C:/Users/tomasz/development/spring-xd/xd/lib</springxd-lib-dir>
//    <springXDURL>http://localhost:9393</springXDURL>
//    <httpPort>9000</httpPort>
//    <httpURL>http://localhost:${httpPort}</httpURL>
//    <dbURL>jdbc:hsqldb:hsql://localhost:9101/xdjob;sql.enforce_strict_size=true;hsqldb.tx=mvccs</dbURL>
//    <dbDriver>org.hsqldb.jdbcDriver</dbDriver>
//    <dbUserName>sa</dbUserName>
//    <dbPassword></dbPassword>
//    <targetResourcesPath>${basedir}/src/test/resources/</targetResourcesPath>
//    <scanDirPath>${targetResourcesPath}scanDir/</scanDirPath>
//    <targetResource>${targetResourcesPath}</targetResource>
//    <targetResourcesURL>file:///${targetResource}</targetResourcesURL>
//    <targetScanPath>${scanDirPath}</targetScanPath>
//    </properties>
//    </profile>
//    <profile>
//    <id>clouderaVM</id>
//    <properties>
//    <springXDURL>http://localhost:9393</springXDURL>
//    <httpPort>9001</httpPort>
//    <httpURL>http://localhost:${httpPort}</httpURL>
//    <dbURL>jdbc:mysql://localhost:3306/SpringBatch</dbURL>
//    <dbDriver>com.mysql.jdbc.Driver</dbDriver>
//    <dbUserName>batch</dbUserName>
//    <dbPassword>batch</dbPassword>
//    <targetResourcesPath>C:/Users/tomasz/VirtualBoxShare/</targetResourcesPath>
//    <scanDirPath>${targetResourcesPath}scanDir/</scanDirPath>
//    <targetResource>/media/sf_VirtualBoxShare/</targetResource>
//    <targetResourcesURL>file://${targetResource}</targetResourcesURL>
//    <targetScanPath>${targetResource}scanDir/</targetScanPath>
//    </properties>
//    </profile>
    @Before
    public void setUp() throws URISyntaxException, IOException, DatabaseUnitException, SQLException, ClassNotFoundException {

        String springXDURL = System.getProperty("springXDURL");
        System.out.println("springXDURL = " + springXDURL);
        httpPort = Integer.parseInt(System.getProperty("httpPort"));
        System.out.println("httpPort = " + httpPort);
        httpURL = System.getProperty("httpURL");


//        private static final int HTTP_PORT = 9001;
//        private static final String HTTP_URL = "http://localhost:" + HTTP_PORT;

        System.out.println("Connecting to Spring XD Server ... ");
        setPaths();
        xdTemplate = new SpringXDTemplate(new URI(springXDURL));
        PagedResources<DetailedContainerResource> containers = xdTemplate.runtimeOperations().listContainers();
        assertNotNull("Cannot retrieve list of SpringXD containers! ", containers);
        System.out.println("Stopping, undeploying and destroying Spring XD Server jobs and streams ... ");
        resetXDState();
        System.out.println("Removing files from scanned dir  ... ");
        resetFiles();
        System.out.println("Resetting database state  ... ");
        databaseConnection = getConnection();
        resetDatabaseState();
    }

    private void setPaths() throws MalformedURLException {
        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + path);
        localResourcesPath = path + "/src/test/resources/";
        System.out.println("Local resource path is: " + localResourcesPath);

       // resourcesURL = new File(resourcesPath).toURI().toURL().toString();
        //System.out.println("Current resource URL is: " + resourcesURL);
        System.out.println("Current scan dir path (filejdbc) is: " + scanDirPath);
    }

    private void resetDatabaseState() throws SQLException {
        Statement stmt = jdbcConnection.createStatement();
        boolean res = stmt.execute("DROP TABLE IF EXISTS BABY_NAMES");
        res = stmt.execute("DROP TABLE IF EXISTS INT_GROUP_TO_MESSAGE");
        res = stmt.execute("DROP TABLE IF EXISTS INT_MESSAGE_GROUP");
        res = stmt.execute("DROP TABLE IF EXISTS INT_MESSAGE");
      //  res = stmt.execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
     //   jdbcConnection.commit();
        // DatabaseOperation.CLEAN_INSERT.
        String createBabyNamesTable = "CREATE TABLE BABY_NAMES(" +
                "YEAR VARCHAR(2000)," +
                "FNAME VARCHAR(2000)," +
                "PERCENT VARCHAR(2000)," +
                "SEX VARCHAR(2000) )";
        res = stmt.execute(createBabyNamesTable);

        String createAggregatorMessageTable = "CREATE TABLE INT_MESSAGE (" +
                "MESSAGE_ID CHAR(36) NOT NULL," +
                "REGION VARCHAR(100) NOT NULL," +
                "CREATED_DATE TIMESTAMP NOT NULL," +
                "MESSAGE_BYTES VARBINARY(10000)," +
                "CONSTRAINT MESSAGE_PK PRIMARY KEY (MESSAGE_ID, REGION));";
        res = stmt.execute(createAggregatorMessageTable);
        res = stmt.execute("CREATE INDEX INT_MESSAGE_IX1 ON INT_MESSAGE (CREATED_DATE)");

        String createAggregatorGroupTable = "CREATE TABLE INT_MESSAGE_GROUP(" +
                "GROUP_KEY CHAR(36) NOT NULL," +
                "REGION VARCHAR(100) NOT NULL," +
                "MARKED BIGINT," +
                "COMPLETE BIGINT," +
                "LAST_RELEASED_SEQUENCE BIGINT," +
                "CREATED_DATE TIMESTAMP NOT NULL," +
                "UPDATED_DATE TIMESTAMP," +
                "CONSTRAINT MESSAGE_GROUP_PK PRIMARY KEY (GROUP_KEY, REGION))";
        res = stmt.execute(createAggregatorGroupTable);


        String createAggregatorGroupToMessageTable = "CREATE TABLE INT_GROUP_TO_MESSAGE(" +
                "GROUP_KEY CHAR(36) NOT NULL," +
                "MESSAGE_ID CHAR(36) NOT NULL," +
                "REGION VARCHAR(100) NOT NULL," +
                "CONSTRAINT GROUP_TO_MESSAGE_PK PRIMARY KEY (GROUP_KEY, MESSAGE_ID, REGION))";

        res = stmt.execute(createAggregatorGroupToMessageTable);

        stmt.close();
    }


    private void resetFiles() throws IOException {

        if (new File(scanDirPath + "Hello.txt").exists())
            Files.delete(Paths.get(scanDirPath + "Hello.txt"));
        if (new File(scanDirPath + "ExclamationMark.txt").exists())
            Files.delete(Paths.get(scanDirPath + "ExclamationMark.txt"));


        if (new File(targetResourcesPath + BABY_NAMES1_CSV).exists() == false)
            Files.copy(Paths.get(localResourcesPath + BABY_NAMES1_CSV), Paths.get(targetResourcesPath + BABY_NAMES1_CSV),StandardCopyOption.COPY_ATTRIBUTES);

        if (new File(targetResourcesPath + BABY_NAMES2_CSV).exists() == false)
            Files.copy(Paths.get(localResourcesPath + BABY_NAMES2_CSV), Paths.get(targetResourcesPath + BABY_NAMES2_CSV),StandardCopyOption.COPY_ATTRIBUTES);

        if (new File(targetResourcesPath + BABY_NAMES3_CSV).exists() == false)
            Files.copy(Paths.get(localResourcesPath + BABY_NAMES3_CSV), Paths.get(targetResourcesPath + BABY_NAMES3_CSV),StandardCopyOption.COPY_ATTRIBUTES);

        if (new File(targetResourcesPath + BABY_NAMES4_CSV).exists() == false)
            Files.copy(Paths.get(localResourcesPath + BABY_NAMES4_CSV), Paths.get(targetResourcesPath + BABY_NAMES4_CSV),StandardCopyOption.COPY_ATTRIBUTES);

        if (new File(targetResourcesPath + BABY_NAMES5_CSV).exists() == false)
            Files.copy(Paths.get(localResourcesPath + BABY_NAMES5_CSV), Paths.get(targetResourcesPath + BABY_NAMES5_CSV),StandardCopyOption.COPY_ATTRIBUTES);
    }

    private void resetXDState() {
        xdTemplate.jobOperations().stopAllJobExecutions();

        xdTemplate.jobOperations().undeployAll();
        xdTemplate.jobOperations().destroyAll();

        xdTemplate.streamOperations().undeployAll();
        xdTemplate.streamOperations().destroyAll();
    }

    protected IDatabaseConnection getConnection() throws DatabaseUnitException, SQLException, ClassNotFoundException {
        Class.forName(SPRING_XD_JDBC_DRIVER);
        jdbcConnection = DriverManager.getConnection(
                SPRING_XD_DB_URL, SPRING_XD_DB_USER_NAME, SPRING_XD_DB_PASSWORD);
        jdbcConnection.setAutoCommit(true);
        return new DatabaseConnection(jdbcConnection);
    }

    /**
     * Load the data which will be compared with test results * @return IDataSet
     */
    protected IDataSet getDataSet() throws Exception {
        CsvDataSet loadedDataSet = new CsvDataSet(new File(localResourcesPath));
        return loadedDataSet;
    }


    @After
    public void tearDown() throws SQLException {
        jdbcConnection.close();
      //  resetXDState();
    }

    @Test
    public void testMultipleEntryPointWorkflow() throws InterruptedException, TimeoutException, ExecutionException, IOException {

        System.out.println("Adding jobs ... ");

        Map<String, String> jobNameToFileUrl = new HashMap<>();
        jobNameToFileUrl.put(BABY_NAMES_1, targetResourcesURL + BABY_NAMES1_CSV);
        jobNameToFileUrl.put(BABY_NAMES_2, targetResourcesURL + BABY_NAMES2_CSV);
        jobNameToFileUrl.put(BABY_NAMES_3, targetResourcesURL + BABY_NAMES3_CSV);
        jobNameToFileUrl.put(BABY_NAMES_4, targetResourcesURL + BABY_NAMES4_CSV);
        jobNameToFileUrl.put(BABY_NAMES_5, targetResourcesURL + BABY_NAMES5_CSV);

        createBabyNamesJobs(jobNameToFileUrl);

        System.out.println("Adding streams consisting of the trigger and the job ... ");
        File fileDir = new File(targetResourcesPath, "scanDir");


        assertTrue(fileDir.exists());

        sendWordByFileAvailabilityToStartTheJob(/*fileDir.toURI().toURL().toString()*/targetScanPath, "HelloFileStream", "Hello", BABY_NAMES_1);

        sendWordByHttpToStartTheJob("WorldHttpStream", "World", BABY_NAMES_2, httpPort);

        sendWordByFileAvailabilityToStartTheJob(/*fileDir.toURI().toURL().toString()*/targetScanPath, "ExclamationMarkStream", "!", BABY_NAMES_3);


        System.out.println("Adding streams defining branches ... ");

        createJobTabListeningStream("Branch1Stream", BABY_NAMES_1, "@correlationIdCounter1.getAndIncrement()", BABY_NAMES_4, SPRING_XD_DB_URL /*+ ";sql.enforce_strict_size=true;hsqldb.tx=mvcc"*/, SPRING_XD_JDBC_DRIVER, SPRING_XD_DB_USER_NAME, SPRING_XD_DB_PASSWORD);

        createJobTabListeningStream("Branch2Stream", BABY_NAMES_2, "@correlationIdCounter2.getAndIncrement()", BABY_NAMES_4, SPRING_XD_DB_URL /*+ ";sql.enforce_strict_size=true;hsqldb.tx=mvcc"*/, SPRING_XD_JDBC_DRIVER, SPRING_XD_DB_USER_NAME, SPRING_XD_DB_PASSWORD);

        createJobTabListeningStream("Branch3Stream", BABY_NAMES_3, "@correlationIdCounter3.getAndIncrement()", BABY_NAMES_4, SPRING_XD_DB_URL/* + ";sql.enforce_strict_size=true;hsqldb.tx=mvcc"*/, SPRING_XD_JDBC_DRIVER, SPRING_XD_DB_USER_NAME, SPRING_XD_DB_PASSWORD);

        String streamName = "Branch4Stream";
        xdTemplate.streamOperations().createStream(streamName, "tap:job:" + BABY_NAMES_1 + ".job > filter --expression='payload instanceof T(org.springframework.batch.core.JobExecution) && payload.getExitStatus()==T(org.springframework.batch.core.ExitStatus).COMPLETED'  --outputType=text/plain  |  transform --expression='new java.util.HashMap()' > queue:job:" + BABY_NAMES_5, true);
        System.out.println("Stream " + streamName + "listening to job events from " + BABY_NAMES_1 + " job to be finished to start " + BABY_NAMES_5);

        Thread.currentThread().sleep(10000);

        System.out.println("Copying file with 'Hello' content to trigger Branch1Stream ...");
        Files.copy(Paths.get(localResourcesPath + "Hello.txt"), Paths.get(scanDirPath + "Hello.txt"), StandardCopyOption.COPY_ATTRIBUTES);
        Thread.currentThread().sleep(10000);

        System.out.println("Posting to " + httpURL + " 'World' payload to trigger Branch2Stream ...");
        sendHttpPayload(httpURL, "World");
        Thread.currentThread().sleep(10000);

        System.out.println("Copying file with '!' content to trigger Branch3Stream ...");
        Files.copy(Paths.get(localResourcesPath + "ExclamationMark.txt"), Paths.get(scanDirPath + "ExclamationMark.txt"), StandardCopyOption.COPY_ATTRIBUTES);
    }

    private void createJobTabListeningStream(String streamName, String startJobName, String correlationIdSpEL, String endJobName, String jdbcURL, String jdbDriver, String dbUserName, String dbPassword) {
        StreamDefinitionResource branch1Stream = xdTemplate.streamOperations().createStream(streamName, "tap:job:" + startJobName + ".job > filter --expression='payload instanceof T(org.springframework.batch.core.JobExecution) && payload.getExitStatus()==T(org.springframework.batch.core.ExitStatus).COMPLETED'  --outputType=text/plain | header-enricher --headers={\"correlationId\":\"" + correlationIdSpEL + "\"} | aggregator --correlation='headers.get(\"correlationId\")' --timeout=36000000 --release='size() eq 3' --store=jdbc --url='" + jdbcURL + "' --driverClassName='" + jdbDriver + "' --username='" + dbUserName + "' --password='" + dbPassword + "' --aggregation=T(org.springframework.util.StringUtils).collectionToDelimitedString(#this.![payload],' ') --outputType=text/plain |  transform --expression='new java.util.HashMap()' > queue:job:" + endJobName, true);
        System.out.println("Stream " + streamName + "listening to job events from " + startJobName + " job and waiting for 3 jobs to be finished to start " + endJobName + " added and deployed !");
    }

    private void sendHttpPayload(String url, String payload) {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        // Fire a request and do not wait for results !
        Future<Void> futureResult = executor.submit(new Request(url, payload));
        executor.shutdown();
    }


    private void sendWordByFileAvailabilityToStartTheJob(String directory, String streamName, String word, String jobName) {
        StreamDefinitionResource fileStream = xdTemplate.streamOperations().createStream(streamName,
                "file --dir='" + directory + "' --outputType=text/plain | filter --expression='payload.equals(\"" + word + "\")' --outputType=text/plain |  transform --expression='new java.util.HashMap()' > queue:job:" + jobName, true);
        System.out.println("Stream " + streamName + " starting job " + jobName + " added and deployed !");
    }

    private StreamDefinitionResource sendWordByHttpToStartTheJob(String streamName, String payload, String jobName, int port) {
        StreamDefinitionResource worldHttpStream = xdTemplate.streamOperations().createStream(streamName,
                "http --port=" + port + " --outputType=text/plain | filter --expression='payload.equals(\"" + payload + "\")' --outputType=text/plain |  transform --expression='new java.util.HashMap()' > queue:job:" + jobName, true);
        System.out.println("Stream " + streamName + " starting job " + jobName + " added and deployed !");
        return worldHttpStream;
    }


    private void createBabyNamesImportJob(String jobName, final String fileURL) {
        defineCSVFileInternalDbImportJob(jobName, fileURL, "year,fname,percent,sex", "BABY_NAMES", ",");
    }

    private void defineCSVFileInternalDbImportJob(String jobName, String fileURL, String columnNames, String tableName, String delimiter) {
        xdTemplate.jobOperations().createJob(jobName, "filejdbc --delimiter=" + delimiter + " --names='" + columnNames + "' --resources='" + fileURL + "' --initializeDatabase=false --tableName=" + tableName + " --makeUnique=true", true);
        System.out.println("Job added and deployed : " + jobName);
    }

    private void createBabyNamesJobs(final Map<String, String> jobNameToFileUrl) {
        for (String jobName : jobNameToFileUrl.keySet()) {
            createBabyNamesImportJob(jobName, jobNameToFileUrl.get(jobName));
        }
    }


    private static class Request implements Callable<Void> {
        private String url;
        private String payload;

        public Request(String url, String payload) {
            this.url = url;
            this.payload = payload;
        }

        private void sendData(String data) {
            HttpClient httpclient = new HttpClient();
            StringRequestEntity requestEntity;
            try {
                requestEntity = new StringRequestEntity(data, "text/plain", "UTF-8");
                PostMethod postMethod = new PostMethod(url);
                postMethod.setRequestEntity(requestEntity);
                httpclient.executeMethod(postMethod);

            } catch (Exception e) {
                System.out.println("Failed to send data " + e);
            }
        }

        @Override
        public Void call() throws Exception {
            sendData(payload);
            return null;
        }
    }


//    private Map<String,Integer> getJobDefinitions() {
//        Map<String,Integer> jobNameToId = new HashMap<>();
//        JobOperations jobOperations = xdTemplate.jobOperations();
//        Iterator<JobDefinitionResource> listOgJobDefinitionsIt = jobOperations.list().iterator();
//        while (listOgJobDefinitionsIt.hasNext()) {
//            JobDefinitionResource jobDefinitionResource =   listOgJobDefinitionsIt.next();
//            jobNameToId.put(jobDefinitionResource.getName(),linkToInteger(jobDefinitionResource.getId()));
//        }
//        return  jobNameToId;
//    }

//    private Integer linkToInteger(Link link) throws  IOException {
////        List<HttpMessageConverter<?>> converters = new LinkedList<>();
////        converters.add(new MappingJackson2HttpMessageConverter());
////        RestTemplate template = new RestTemplate(converters);
////// or use the set method
////        template.setMessageConverters(converters);
////// make url call
////        Resource<EmployeeDTO> resource = template.getForObject("url", Resource.class);
//
////        ObjectMapper objectMapper = new ObjectMapper();
////        objectMapper.registerModule(new Jackson2HalModule());
////        RestTemplate restTemplate= new RestTemplate();
////        String resultBody = restTemplate.getForObject(link.getHref(), String.class);
////        Integer id = objectMapper.readValue(resultBody, Integer.class);
////        return id;
//    }


    // ExitStatus result = future.get(120, TimeUnit.SECONDS);


/*
    private ExitStatus waitForJobCompletion() throws InterruptedException, java.util.concurrent.TimeoutException, java.util.concurrent.ExecutionException {
        final SettableFuture<ExitStatus> future = SettableFuture.create();

        new Thread(new Runnable(){

            public void run(){

                JobOperations jobOperations = xdTemplate.jobOperations();

                for (JobExecutionInfoResource jobExecutionInfoResource : jobOperations.listJobExecutions()) {
                    JobExecution jobExecution = jobExecutionInfoResource.getJobExecution();
                    if (jobExecution.getExitStatus() != ExitStatus.EXECUTING) {
                        future.set(jobExecution.getExitStatus());
                    } else {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
//
//                    jobExecutionInfoResource.getJobId();
//                    jobExecutionInfoResource.getExecutionId();
//                    jobExecutionInfoResource.getDuration();
//                    jobExecutionInfoResource.getName()

                  // jobOperations.stopJobExecution();
                }
               // future.set("Hello world!!!");
            }

        }).start();

        ExitStatus result = future.get(120, TimeUnit.SECONDS);
        return result;
    }*/

}
