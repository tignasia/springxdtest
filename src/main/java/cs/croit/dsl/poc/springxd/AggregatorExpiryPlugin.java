package cs.croit.dsl.poc.springxd;


import org.apache.commons.logging.Log;
import org.springframework.stereotype.Component;
import org.springframework.xd.module.ModuleType;
import org.springframework.xd.module.core.Module;
import org.springframework.xd.module.core.Plugin;

import java.util.Properties;

import static org.apache.commons.logging.LogFactory.getLog;


@Component
public class AggregatorExpiryPlugin implements Plugin {

    private final static Log logger = getLog(AggregatorExpiryPlugin.class);

    public static final String XD_GROUP_NAME_KEY = "xd.group.name";

    public void preProcessModule(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("preProcessModule(" + module + ") before : " + module.getProperties());
        Properties properties = new Properties();
        properties.setProperty(XD_GROUP_NAME_KEY, module.getDescriptor().getGroup());
        properties.setProperty("send-partial-result-on-expiry", "false");
        properties.setProperty("expire-groups-upon-timeout", "false");
        module.addProperties(properties);
        if (logger.isDebugEnabled())
            logger.debug("preProcessModule(" + module + ") after : " + module.getProperties());
    }

    public void postProcessModule(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("postProcessModule(" + module + ")");
    }

    public void removeModule(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("removeModule(" + module + ")");
    }

    public void beforeShutdown(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("beforeShutdown(" + module + ")");
    }

    public boolean supports(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("supports(" + module + ")");
        ModuleType moduleType = module.getType();
        // logger.debug("Supports module type : " + moduleType);
        boolean supports = (moduleType == ModuleType.processor && module.getName().equals("aggregator"));
        if (logger.isDebugEnabled())
            logger.debug("Supports module " + module.getName() + " = " + supports);
        return supports;
    }
}
