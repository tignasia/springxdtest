package cs.croit.dsl.poc.springxd;


import org.apache.commons.logging.Log;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.stereotype.Component;
import org.springframework.xd.module.ModuleType;
import org.springframework.xd.module.core.Module;
import org.springframework.xd.module.core.Plugin;

import java.util.Properties;

import static org.apache.commons.logging.LogFactory.getLog;

//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.xd.dirt.util.ConfigLocations;
//import org.springframework.xd.module.core.Module;
//import static org.springframework.xd.module.options.spi.ModulePlaceholders.XD_GROUP_NAME_KEY;
@Component
public class CorrelationCountersPlugin implements Plugin {

    private final static Log logger = getLog(CorrelationCountersPlugin.class);

    static {
        logger.debug("CorrelationCountersPlugin LOADED !!!!");
    }
    //  https://github.com/spring-projects/spring-xd/issues/1158

    protected static final String PLUGIN_CONTEXT_CONFIG_ROOT = "META-INF/spring-xd/plugins/hecc/";
    public static final String XD_GROUP_NAME_KEY = "xd.group.name";

    public void preProcessModule(Module module) {
        logger.debug("preProcessModule(" + module + ")");

        Properties properties = new Properties();
        properties.setProperty(XD_GROUP_NAME_KEY, module.getDescriptor().getGroup());
        module.addProperties(properties);
        // Either java based  or xml configuration can be used
        module.addSource(CorrelationCountersPluginAppConfig.class /*new ClassPathResource(PLUGIN_CONTEXT_CONFIG_ROOT + "correlation_counters.xml")*/);
        // Here is how to add  beans programmatically
        //        module.addListener(new ApplicationListener<ApplicationPreparedEvent>() {
        //            @Override
        //            public void onApplicationEvent(ApplicationPreparedEvent event) {
        //                ConfigurableListableBeanFactory beanFactory = event.getApplicationContext().getBeanFactory();
        //                beanFactory.addBeanPostProcessor(new HeaderEnricherBeanPostProcessor(beanFactory));
        //            }
        //
        //        });

    }

    public void postProcessModule(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("postProcessModule(" + module + ")");
    }

    public void removeModule(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("removeModule(" + module + ")");
    }

    public void beforeShutdown(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("beforeShutdown(" + module + ")");
    }

    public boolean supports(Module module) {
        if (logger.isDebugEnabled())
            logger.debug("supports(" + module + ")");
        ModuleType moduleType = module.getType();
        // logger.debug("Supports module type : " + moduleType);
        boolean supports = (moduleType == ModuleType.processor && module.getName().equals("header-enricher"));
        if (logger.isDebugEnabled())
            logger.debug("Supports module " + module.getName() + " = " + supports);
        return supports;
    }

    private static class HeaderEnricherBeanPostProcessor implements BeanPostProcessor/*, BeanFactoryAware*/ {
        private ConfigurableListableBeanFactory beanFactory;

        public HeaderEnricherBeanPostProcessor(ConfigurableListableBeanFactory beanFactory) {
            this.beanFactory = beanFactory;
        }

        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
            return bean;
        }

        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
            if (logger.isDebugEnabled())
                logger.debug("postProcessAfterInitialization(" + bean + "," + beanName + ")");
            BeanDefinitionRegistry registry = ((BeanDefinitionRegistry) beanFactory);
            registerBeanDefinition(registry, "correlationIdCounter1");
            registerBeanDefinition(registry, "correlationIdCounter2");
            registerBeanDefinition(registry, "correlationIdCounter3");
            return bean;
        }

        private void registerBeanDefinition(BeanDefinitionRegistry registry, String name) {
            if (logger.isDebugEnabled())
                logger.debug("registerBeanDefinition(" + name + ") .... ");
            GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
            beanDefinition.setBeanClass(java.util.concurrent.atomic.AtomicInteger.class);
            beanDefinition.setLazyInit(false);
            beanDefinition.setAbstract(false);
            beanDefinition.setAutowireCandidate(true);
            beanDefinition.setScope("singleton");
            registry.registerBeanDefinition(name, beanDefinition);
            if (logger.isDebugEnabled())
                logger.debug("registerBeanDefinition(" + name + ")  finished !");
        }

    }
}
