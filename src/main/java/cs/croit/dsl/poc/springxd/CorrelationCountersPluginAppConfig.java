package cs.croit.dsl.poc.springxd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@ComponentScan({"cs.croit.dsl.poc.springxd"})
public class CorrelationCountersPluginAppConfig {

	@Bean(name = "correlationIdCounter1")
    @Lazy(value = false)
    @Scope(value = "singleton")
    @Autowired
	public AtomicInteger getCorrelationIdCounter1() {
       return new AtomicInteger();
	}

	@Bean(name = "correlationIdCounter2")
    @Lazy(value = false)
    @Scope(value = "singleton")
    @Autowired
	public AtomicInteger getCorrelationIdCounter2() {
        return new AtomicInteger();
	}

	@Bean(name = "correlationIdCounter3")
    @Lazy(value = false)
    @Scope(value = "singleton")
    @Autowired
	public AtomicInteger getCorrelationIdCounter3() {
        return new AtomicInteger();
	}

}
